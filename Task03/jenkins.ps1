docker volume create jenkins_data
docker run -d -p 50000:50000 -p 80:8080 --name=jenkins --restart=always -u root --privileged -v /usr/bin/docker:/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock -v jenkins_data:/var/jenkins_home jenkins/jenkins:lts
Start-Sleep -Seconds 300
Write-Host "Initial Admin Password:"
docker exec -t jenkins cat /var/jenkins_home/secrets/initialAdminPassword