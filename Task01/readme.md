# Task 1 - Automated install of Docker Desktop

## Instructions

1. Create a folder of your choice and put the scripts in it
2. Open an elevated command prompt and change directory into the scripts folder of yours
3. Execute docker-install.cmd

The docker-intall.cmd will start a powershell script what download the necessary components also enable the required Windows features. It will reboot the machine. The Docker Desktop is installed automatically after reboot, when you login into the machine.

The docker desktop will only be available after logout and log back in.

## Note
If you intend to run the process above inside a Hyper-V VM, you should enable nested virtualization on your VM with the following (elevated) powershell command:

`Set-VMProcessor -VMName <VM Name> -ExposeVirtualizationExtensions $true`

## Room for Improvement
- Use temp folder instead of c:\inst for the downloaded artifacts
- Cleanup after the script
- Create a temporary admin user with autologin enabled to eliminate the login requirement between the reboot phases (delete after the process complette)