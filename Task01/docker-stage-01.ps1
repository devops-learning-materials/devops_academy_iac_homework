mkdir c:\inst
Invoke-WebRequest https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi -o c:\inst\wsl_update_x64.msi
Invoke-WebRequest https://desktop.docker.com/win/stable/amd64/Docker%20Desktop%20Installer.exe -o c:\inst\docker_install.exe
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux,VirtualMachinePlatform -NoRestart

$stage2script = '-ExecutionPolicy Bypass -WindowStyle Normal -NoLogo -NoProfile -File "' + $PSScriptRoot + '\docker-stage-02.ps1"'
Get-ScheduledTask -TaskName DockerStage2Task -EA SilentlyContinue | Unregister-ScheduledTask -Confirm:$false
$act = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument $stage2script
$trig = New-ScheduledTaskTrigger -AtLogOn -RandomDelay 00:00:55
Register-ScheduledTask -TaskName DockerStage2Task -Action $act -Trigger $trig -RunLevel Highest

Restart-Computer