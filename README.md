---
title: Wordpress -- DevOps way
---

# Preface:

Installing a Wordpress CMS with a MySQL RDBMS backend a straightforward
but long and boring manual task. It has several requirements.

<https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-ubuntu-20-04-with-a-lamp-stack>

On the other side the process (as all manual processes) give room for
errors. The repeatability, upgradeability, backups, high availability
are the factors, what is missing from the process.

As DevOps people, we are committed to principles like IaC and
automation.

The tasks described bellow lead us to a working Wordpress site, but
built around the DevOps tools. The goal is to utilize most of the DevOps
toolset we use today.

The design of the task is based on a simple requirement. Be able to
build it on a single Windows 10 Based machine (regular EPAM desktop or
laptop), utilizing AWS Free Tier Account. We are mainly using Docker
based and PaaS solutions.

# Task 1 - Setup the local environment

Write a script (Powershell recommended but not required), what can
install Docker Desktop on your machine with WSL2 backend unattended. It
also needs to install all the Microsoft Windows components required for
this.

Hint: If you install a Windows 10 inside the Hyper-V (or Oracle
Virtualbox), you can easily test your script utilizing the snapshot
feature

## Task 2 - Setup the cloud services

Register an AWS and a GitLab account. In the AWS also setup an IAM User,
with adrministrative permissions and access keypair (it will be used by
the AWS based deployments). In the GitLab setup an SSH key for yourself,
a deployment token for the GitLab container registry, with write
permission.

Hint:

For the AWS registration don’t use any of you regular e-mail addresses. The AWS stick your e-mail to the account you create. It will be kept reserved even if you delete the account. You will never be able to use that e-mail for AWS registration ever again. Register a disposable address at gmail.com, outlook.com, etc.
It is recommended to use virtual bank card created for this purpose, to handle the risks of overutilization of the AWS resources.

## Task 3 - Install Jenkins

Write a script (Powershell recommended), what is able to deploy a
Jenkins Master docker container into the local docker we installed in
the Task 1.

Requirements:

-   Use the publicly available current Jenkins docker image
    <https://hub.docker.com/r/jenkins/jenkins>

-   The Jenkins should be setup on the way to be able to build docker
    images inside (docker in the docker scenario)

-   Jenkins using local filesystem as data storage. Use a persistent
    storage for it (docker persistent volume)

# Task 4 - Create Jenkins Agent

Write a Jenkins job what is able to build a docker image for your linux
Jenkins agent machine.

Requirements:

-   The job should be a pipeline job.

-   The job should be written in declarative pipeline script.

-   The job source should be kept in a GitLab repository

-   Jenkins should access the GitLab repository above using the
    credential manager, using GitLab deploy key

-   The agent docker can be built from the official Jenkins agent image
    (or from starch) <https://hub.docker.com/r/jenkins/inbound-agent/>

-   The image should contain the following tools preinstalled: AWS CLI,
    Terraform

-   The Dockerfile and related components should be kept in the GitLab

-   The job should push the ready image into the GitLab container
    registry, using the deploy token created earlier, utilizing the
    Jenkinks credential manager

Write a Jenkins job (can be in the same job above) to deploy the above created image into the local
docker infrastructure

Requirements:

-   The job should be a pipeline job.

-   The job should be written in declarative pipeline script.

-   The job source should be kept in a GitLab repository

-   Jenkins should access the GitLab repository above using the
    credential manager, using GitLab deploy key

-   Optional: Handle the automated registration of the agent into the
    Jenkins Master

# Task 5 -- AWS Prerequisites

We will use Terraform for the later deployment tasks. Terraform need a location to store its state files. In AWS the best practice is to use s3 bucket for it
Create a Jenkins job what able to create the s3 bucket for the Terraform states

Reqiurements:

-	The job should be a pipeline job.

-	The job should be written in declarative pipeline script.

-	The job source should be kept in a GitLab repository

-	Jenkins should access the GitLab repository above using the credential manager, using GitLab deploy key

-	Use AWS CLI directly or utilize AWS CloudFormation to create the S3 bucket (store everything related code in the GitLab repository

Hint:

This is the first time we are creating AWS resources. Some of part of the AWS is provided freely (Free Tier) for the users. The AWS is not a child playground. Will not hold your hand if you do something terribly wrong, just charge it on your card. 
So, from now on it is highly recommended to closely monitor the AWS Billing for your costs. Also, you can setup a Cloudwatch Alarm for the above $0 spending.

# Task 6 – Networking

Now we are entering into the Terraform’s world.
The upcoming tasks will use a Jenkins job to deploy Terraform code into the AWS environment. As the reusable code always help to reduce the necessary work, the task here is create a Jenkins job can be used in the following tasks, without modifications.
Create a Jenkins job, able to run Terraform

Requirements:

-	The job should be a pipeline job.

-	The job should be written in declarative pipeline script.

-	The job source should be kept in a GitLab repository

-	Jenkins should access the GitLab repository above using the credential manager, using GitLab deploy key

-	The job should take the following two parameters:

    -	Terraform command (plan, apply, destroy, etc.)

    -	A folder name where the terraform code reside

Create a folder for the terraform code. This will contain the Networking part.
In the folder write a Terraform code able to deploy a separate VPC for our efforts. Also generate a subnet with the necessary routing, DHCP, etc. resources will be able to hold our complete WordPress stack.
Additional requirement: The identifying data of the network stack should be published into the AWS SSM Parameter Store. This will allow you to use this data in the other Terraform stacks.

# Task 7 – Database

The WordPress uses two places for storing data. One is the filesystem, the other is an RDBMS database. AWS has a PaaS RDMS offering. In this task we should utilize it.
Create a terraform folder and code in it for deploying an Amazon Aurora database in the AWS RDS
Requirements:

-	It must use MySQL flavor

-	It should be deployed into the subnet created above

-	It should use identifying data from the AWS SSM Parameter store or from the AWS Secret Manager

# Task 8 – ECS Cluster, EFS and ALB

# Task 9 - ASG and Launch configuration

# Task 10 - WordPress Service

# Task 11 – Backup

(script in docker + S3 bucket), Optional Retention

# Optional tasks

phpMyAdmin
WAF – OWASP 10
CloudFront
