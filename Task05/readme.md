# Task 5 – Bootstratp the Terraform

## Instructions
- Create a GitLab repo for the Terraform files (this repo will be used later for the subsequent tasks)
- Commit the Jenkins_bootstrap file into the repo
- Create a Jenkins job pointing to the file above
- Deploy the bucket with the job

## Note
The Docker Desktop WSL2 backend has an issue with the time sync. It maybe need to handled, if the AWS CLI give back errors.