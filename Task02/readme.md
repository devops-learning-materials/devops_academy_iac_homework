# Task 2 - Register at online providers

1. Go to gmail.com and register an e-mail
2. Go to [gitlab.com](https://gitlab.com/users/sign_up) and create an account (for simplicity you can use your previously created google account)
3. Create and register an SSH key for gitlab. You can use PuTTYGen, or OpenSSH for this.

![puttygen](PuTTYGen.JPG "puttygen")

![ssh](SSH.JPG "ssh")

4. Create a token for GitLab container registry

![token](DockerToken.JPG "token")

5. Register an AWS Account with the e-mail above: [AWS](https://portal.aws.amazon.com/billing/signup)
6. Login with the root account and create an IAM user with administrative permissions

![AWS1](AWSAdmin1.JPG "AWS1")

![AWS2](AWSAdmin2.JPG "AWS2")

![AWS5](AWSAdmin5.JPG "AWS5")

It is important to save the CSV now, as it contain the programmatic access codes, and will not be shown later



