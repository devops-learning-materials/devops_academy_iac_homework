terraform {
  backend "s3" {
    bucket = "devopsacademy-tfstate"
    key    = "network.tfstate"
    region = "eu-west-1"
  }
}