resource "aws_vpc" "wp-vpc" {
    cidr_block="10.0.0.0/20"
    enable_dns_support   = true
    enable_dns_hostnames = true
    tags = {
      Name = "WordPress"
    }   
}

resource "aws_internet_gateway" "wp-vpc" {
  vpc_id = aws_vpc.wp-vpc.id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.wp-vpc.id
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.wp-vpc.id
}

resource "aws_subnet" "public1" {
  vpc_id                  = aws_vpc.wp-vpc.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "eu-west-1a"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "public2" {
  vpc_id                  = aws_vpc.wp-vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "eu-west-1b"
  map_public_ip_on_launch = true
}

resource "aws_route_table_association" "public1" {
  subnet_id      = aws_subnet.public1.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public2" {
  subnet_id      = aws_subnet.public2.id
  route_table_id = aws_route_table.public.id
}

