# Task 4 – Create Jenkins Agent

## Instructions
- Create a GitLab repo for the Jenkins Agent files
- Change the url in the Jenkinsfile header to your repo
- Commit the Dockerfile and the Jenkinsfile from this folder to the repo
- Setup the agent according to the pictures (See bellow)
- Copy out the secret from the agent page (see picture)
- Create a secret text named DockerAgent1Token with the secret above in your Jenkins folder (see picture)
- Setup the Jenkins job according the pictures (See bellow)
- Run the job. After a few minutes you should see your agent connected

## Agent Setup

![Agent01](Agent01.JPG "Agent01.JPG")

![Agent02](Agent02.JPG "Agent02.JPG")

![Agent03](Agent03.JPG "Agent03.JPG")

![Agent04](Agent04.JPG "Agent04.JPG")

## Job Setup

![Job1](Job1.JPG "Job1.JPG")

![Job2](Job2.JPG "Job2.JPG")
